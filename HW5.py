import requests


def currency_wrapper(currency_code):
    match currency_code:
        case 980:
            return "UAH"
        case 840:
            return "USD"
        case 978:
            return "EUR"
        case 826:
            return "GBP"
        case 392:
            return "JPY"
        case 124:
            return "CAD"
        case _:
            return str(currency_code)


def print_data_currency(currency_list, interesting_currency):
    if currency_list:
        for currency in currency_list:
            if currency["currencyCodeA"] in interesting_currency:
                print(
                    f"{currency_wrapper(currency['currencyCodeA'])}: {currency['currencyCodeA']}, {currency_wrapper(currency['currencyCodeB'])}: {currency['currencyCodeB']}")
                print(f"Rate Buy: {currency.get('rateBuy', 'N/A')}")
                print(f"Rate Sell: {currency.get('rateSell', 'N/A')}")
                print(f"Rate Cross: {currency.get('rateCross', 'N/A')}\n")


def exchange_func(currency_list, interesting_currency):
    for currency in currency_list:
        if currency["currencyCodeA"] in interesting_currency:
            print(
                f"{currency_wrapper(currency['currencyCodeA'])}: {currency['currencyCodeA']}, {currency_wrapper(currency['currencyCodeB'])}: {currency['currencyCodeB']}")
            print(f"Rate Buy: {currency.get('rateBuy', 'N/A')}")
            print(f"Rate Sell: {currency.get('rateSell', 'N/A')}")
            print(f"Rate Cross: {currency.get('rateCross', 'N/A')}\n")
            try:
                sum_we_want_to_exchange = float(input(
                    f"Enter how much {currency_wrapper(currency['currencyCodeA'])} you want to exchange into {currency_wrapper(currency['currencyCodeB'])}: "))
                print(
                    f"You will get {sum_we_want_to_exchange * currency.get('rateSell', 0)} {currency_wrapper(currency['currencyCodeB'])}")
            except ValueError:
                print("Please enter a valid amount.")


class CurrencyExchangeTerminal:
    def __init__(self):
        url_api_mono = "https://api.monobank.ua/bank/currency"
        data_currency = requests.get(url_api_mono)
        if data_currency.status_code == 200:
            self.data_currency = data_currency.json()
        else:
            print(f"Failed to get data: {data_currency.status_code}")

    def choose_currency(self):
        if self.data_currency:
            try:
                interesting_currency = list(map(int, input("Enter currency you want to see: ").split(",")))
                print_data_currency(self.data_currency, interesting_currency)
            except ValueError:
                print("Enter valid currency codes separated by commas")

    def top_currency(self):
        if self.data_currency:
            top_currency_list = [840, 978, 124, 392, 826]
            print_data_currency(self.data_currency, top_currency_list)

    def exchange(self):
        if self.data_currency:
            try:
                print("Available currencies:\nUSD - 840\nEUR - 978\nGBR - 826\nJPY - 392\nCAD - 124")
                currency = list(map(int, input("Enter currency you want to exchange (code): ").split(",")))
                exchange_func(self.data_currency, currency)
            except ValueError:
                print("Enter valid currency codes separated by commas")


if __name__ == "__main__":
    needed_currency = CurrencyExchangeTerminal()
    needed_currency.choose_currency()
    needed_currency.top_currency()
    needed_currency.exchange()
