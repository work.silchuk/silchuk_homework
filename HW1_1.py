information = []
title_for_information = ["Enter name\n", "Enter your months salary\n"]
count_of_title = 0
while True:
    information.append(input(title_for_information[count_of_title]))
    for symbol in information[-1]:
        if not symbol.isalnum() or symbol.isdigit():
            print("Invalid enter")
            count_of_title = 0
            break
        else:
            count_of_title += 1
    if count_of_title > 0:
        count_of_title = 1
        information.append(input(title_for_information[count_of_title]))
        for symbol in information[-1]:
            if not symbol.isdigit():
                print("Invalid number")
                count_of_title = 0
                break
            else:
                count_of_title += 1
        if count_of_title > 0:
            break

print(f"Щорічна зарплата {information[0]} становить {int(int(information[1]) * 12 / 1000)} тис. доларів")
