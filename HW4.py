import time


def user_input(number):
    created_list = [odd_number for odd_number in range(3, number, 2)]
    created_list.insert(0, 2)
    return created_list


def ydemo_po_spysku(spysok):
    dilnyk = 3
    index_nastupnogo_dilnyka = 0
    while dilnyk < spysok[-1] // 2 + 1:
        for number in spysok:
            if number % dilnyk == 0 and number / dilnyk != 1:
                spysok.remove(number)
        dilnyk = spysok[index_nastupnogo_dilnyka]
        index_nastupnogo_dilnyka += 1
        print(dilnyk)
    return spysok


def stopwatch():
    start = time.time()
    ydemo_po_spysku(user_input(border))
    finish = time.time()
    return finish - start


def stopwatch_1():
    start = time.time()
    sieve_of_eratosthenes(border)
    finish = time.time()
    return finish - start


def sieve_of_eratosthenes(n):
    primes = [2]
    sieve = [True] * (n + 1)

    for p in range(3, int(n ** 0.5) + 1, 2):
        if sieve[p]:
            primes.append(p)

            for i in range(2 * p, n + 1, p):
                sieve[i] = False

    for p in range(int(n ** 0.5)+1, n+1):
        if sieve[p] and p % 2 != 0:
            primes.append(p)

    return primes
#тут я взяв стандартний код сфери Ератосфена, тому міняти змінні не бачу сенсу

def perevirka(list1, list2):
    index_pokaznyk_dovshzyny = 0
    while index_pokaznyk_dovshzyny != len(list1):
        if list1[index_pokaznyk_dovshzyny] == list2[index_pokaznyk_dovshzyny]:
            print(f'{list1[index_pokaznyk_dovshzyny]} == {list2[index_pokaznyk_dovshzyny]}')
            index_pokaznyk_dovshzyny += 1
        else:
            print(f'{list1[index_pokaznyk_dovshzyny]} != {list2[index_pokaznyk_dovshzyny]}')
            break


border = int(input("Enter number: ".strip()))
perevirka(sieve_of_eratosthenes(border), ydemo_po_spysku(user_input(border)))
print(f"{stopwatch()} My function\n{stopwatch_1()} Eratosphene")
