three_numbers = []
for i in range(0, 3):
    temp = int(input(f"Enter {i+1} number: ").strip())
    three_numbers.append(temp)
three_numbers = sorted(three_numbers)
print(f"Least is {three_numbers[0]}\nAverage is {three_numbers[1]}\nLargest is {three_numbers[2]}")
