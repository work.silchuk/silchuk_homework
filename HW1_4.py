while True:
    status = 0
    while True:
        first_user_number = input("Enter a: ")
        if all(symbol.isdigit() for symbol in first_user_number):
            status = 1
            break
        else:
            print("Invalid number")

    while True:
        second_user_number = input("Enter b: ")
        if all(symbol.isdigit() for symbol in second_user_number):
            status = 2
            break
        else:
            print("Invalid number")

    if status == 2:
        first_user_number = int(first_user_number)
        second_user_number = int(second_user_number)
        print(
                f"Sum is {first_user_number + second_user_number}\n"
                f"Difference is {first_user_number - second_user_number}\n"
                f"Multiplying result is {first_user_number * second_user_number}\n"
                f"Division result is {first_user_number // second_user_number}\n"
                f"Remainder of deviding one by the other is {first_user_number % second_user_number}")
        if first_user_number >= second_user_number:
            print("True")
        else:
            print("False")
        break
