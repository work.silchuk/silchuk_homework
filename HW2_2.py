comparasion_array = []
while True:
    status = 0
    for i in range(0, 2):
        user_number = input(f"Enter {i + 1} number: ")

        if all(symbol.isdigit() for symbol in user_number):
            comparasion_array.append(int(user_number))
            status += 1
        else:
            print("Invalid enter")
            comparasion_array.clear()
            break

    if status == 2:
        comparasion_array.sort()
        print(f"{comparasion_array[0]} is less than {comparasion_array[-1]}"
              f"\n{comparasion_array[-1]} is greater than {comparasion_array[0]}")
        break
