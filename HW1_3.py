while True:
    error = 0
    revers_number = []
    while True:
        user_number = input("Enter number from 100 to 999: ")
        if 100 <= int(user_number) <= 999:
            break
    for digit in user_number:
        if digit.isdigit():
            revers_number.insert(0, digit)
        else:
            error = 1
    if error == 0:
        print(*revers_number, sep="")
        break
