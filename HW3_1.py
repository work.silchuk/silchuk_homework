students = 6
i = 0
students_results = {
    "student_1": {},
    "student_2": {},
    "student_3": {},
    "student_4": {},
    "student_5": {},
    "student_6": {}
}
failed_students_marks = []
passed_students_marks = []
while students != 0:
    students_results[f"student_{i + 1}"]['points'] = int(input(f"{i + 1} student's points: ").strip())
    students_results[f"student_{i + 1}"]['result'] = input("Result (F - failed or P - passed):  ").strip().capitalize()
    if students_results[f"student_{i + 1}"]['result'] == "F" and 0 < students_results[f"student_{i + 1}"]['points'] < 100:
        failed_students_marks.append(students_results[f"student_{i + 1}"]['points'])
        i += 1
        students -= 1
    elif students_results[f"student_{i + 1}"]['result'] == "P" and 0 < students_results[f"student_{i + 1}"]['points'] < 100:
        passed_students_marks.append(students_results[f"student_{i + 1}"]['points'])
        i += 1
        students -= 1
    else:
        print("Try again")

failed_students_marks = sorted(failed_students_marks)
passed_students_marks = sorted(passed_students_marks)
if failed_students_marks[-1] >= passed_students_marks[0]:
    print("Professor was inconsistent!")
elif passed_students_marks[0] > failed_students_marks[-1]:
    print("Professor was consistent")
    print(f"Range of passing is: {failed_students_marks[-1]+1} - {passed_students_marks[0]}")
else:
    print("There is something wrong")
